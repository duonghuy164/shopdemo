@extends('admin.layout.master')


@section('title')
    list brand

@endsection


@section('content')
    <div class="container-fluid">

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add">Add Brand</button>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>ID</th>
                                <th>BRAND_NAME</th>
                                <th>ADDRESS</th>
                                <th>Chỉnh sửa</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach($brand as $key=>$value)
                            <tr>
                                <td>{{ $key +1 }}</td>
                                <td>{{$value->id}}</td>
                                <td>{{$value->brand_name}}</td>
                                <td>{{$value->address}}</td>
                                <td>
                                    <button class="btn btn-primary edit" data-toggle="modal" data-target="#edit" type="button" data-id="{{$value->id}}"><i class="fas fa-edit"></i></button>
                                    <button class="btn btn-danger delete" data-toggle="modal" data-target="#delete" type="button" data-id="{{$value->id}}"><i class="fas fa-trash-alt"></i></button>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

<!-- add modal -->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Chỉnh sửa:<span class="title"></span></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin: 5px">
                        <div class="col-lg-12">
                            <form role="form" >
                                <fieldset class="form-group">
                                    <label>Brand_name</label>
                                    <input class="form-control brand" name="brand" id="brand" >
                                    <span class="error" style="color:green ;font-size: 1rem; "></span>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label>Address</label>
                                    <input class="form-control address" name="address" >
                                    <span class="error" style="color:green ;font-size: 1rem; "></span>
                                </fieldset>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success addBrand">Save</button>
                    <button type="reset" class="btn btn-primary ">Làm Lại</button>

                </div>
            </div>
        </div>
    </div>


<!-- end add modal -->



{{--    edit modal--}}
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Chỉnh sửa:<span class="title"></span></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin: 5px">
                        <div class="col-lg-12">
                            <form role="form" >
                                <fieldset class="form-group">
                                    <label>Brand_name</label>
                                    <input class="form-control brand" name="brand" id="brand" >
                                    <span class="error" style="color:green ;font-size: 1rem; "></span>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label>Address</label>
                                    <input class="form-control address" name="address" id="address" >
                                    <span class="error" style="color:green ;font-size: 1rem; "></span>
                                </fieldset>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success update">Save</button>
                    <button type="reset" class="btn btn-primary ">Làm Lại</button>

                </div>
            </div>
        </div>
    </div>
{{--    end edit modal--}}

{{--    delete modal--}}
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Bạn có muốn xóa ?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" style="margin-left: 183px;">
                    <button type="button" class="btn btn-success delete">Có</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Không</button>
                    <div>
                    </div>
                </div>
            </div>

{{--    end delete modal--}}
@endsection
