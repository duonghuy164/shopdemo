@extends('admin.layout.master')


@section('title')
    add new brand
@endsection


@section('content')
    <div class="card shadow mb-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"> Add New Brand</h6>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row" style="margin: 5px">

                <div class="col-lg-5">
                    <form role="form" action="{{ route('brand.store') }}" method="post">
                        @csrf
                        <fieldset class="form-group">
                            <label>Brand_name</label>
                            <input class="form-control" name="brand" >
                        </fieldset>
                        <fieldset class="form-group">
                            <label>Address</label>
                            <input class="form-control" name ="address">
                        </fieldset>


                        <button type="submit" class="btn btn-success">Add</button>
                        <button type="reset" class="btn btn-primary">Reset</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
