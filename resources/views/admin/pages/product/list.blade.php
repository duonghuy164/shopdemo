@extends('admin.layout.master')


@section('title')
    list brand

@endsection


@section('content')
    <div class="container-fluid">

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"></h6>
                <button type="button" class="btn btn-primary">Add Product</button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>ID</th>
                            <th>NAME</th>
                            <th>Color</th>
                            <th>Brand</th>
                            <th>Price</th>
                            <th>Chỉnh sửa</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($product as $key=>$value)
                            <tr>
                                <td>{{ $key +1 }}</td>
                                <td>{{$value->id}}</td>
                                <td>{{$value->name_product}}</td>
                                <td>{{$value->Color->name_color}}</td>
                                <td>{{$value->Brand->brand_name}}</td>
                                <td>{{$value->price}}</td>
                                <td>
                                    <button class="btn btn-primary editProduct" data-toggle="modal" data-target="#edit" type="button" data-id="{{$value->id}}"><i class="fas fa-edit"></i></button>
                                    <button class="btn btn-danger deleteProduct" data-toggle="modal" data-target="#delete" type="button" data-id="{{$value->id}}"><i class="fas fa-trash-alt"></i></button>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>


    {{--    edit modal--}}
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Chỉnh sửa:<span class="title"></span></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin: 5px">
                        <div class="col-lg-12">
                              <form role="form" >
                        @csrf
                        <span class="error" style="color:green ;font-size: 1rem; "></span>
                        <fieldset class="form-group">
                            <label>Product_name</label>
                            <input class="form-control name" name="name" >
                        </fieldset>
                        <fieldset class="form-group">
                            <label>Color</label>
                            <select class="form-control color" name="color">
                                
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>brand</label>
                           <select class="form-control brand" name="brand">
                               
                           </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>price</label>
                            <input class="form-control price" name ="price">
                        </fieldset>

                    </form>

                        <button type="button" class="btn btn-success updateProduct">Add</button>
                        <button type="reset" class="btn btn-primary">Reset</button>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    {{--    end edit modal--}}

    {{--    delete modal--}}
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Bạn có muốn xóa ?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" style="margin-left: 183px;">
                    <button type="button" class="btn btn-success delete">Có</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Không</button>
                    <div>
                    </div>
                </div>
            </div>

    {{--    end delete modal--}}
@endsection
