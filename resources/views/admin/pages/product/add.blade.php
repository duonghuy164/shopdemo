@extends('admin.layout.master')


@section('title')
    add new Product
@endsection


@section('content')
    <div class="card shadow mb-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"> Add New Product</h6>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row" style="margin: 5px">

                <div class="col-lg-5">
                    <form role="form" action="{{ route('product.store') }}" method="post" >
                        @csrf
                        <fieldset class="form-group">
                            <label>Product_name</label>
                            <input class="form-control" name="name" >
                        </fieldset>
                        <fieldset class="form-group">
                            <label>Color</label>
                            <select class="form-control" name="color">
                                @foreach($color as $col)
                                <option value="{{$col->id}}">{{ $col->name_color }}</option>
                                @endforeach
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>brand</label>
                           <select class="form-control" name="brand">
                               @foreach($brand as $bra)
                               <option value="{{ $bra->id }}">{{ $bra->brand_name }}</option>
                               @endforeach
                           </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>price</label>
                            <input class="form-control" name ="price">
                        </fieldset>


                        <button type="submit" class="btn btn-success">Add</button>
                        <button type="reset" class="btn btn-primary">Reset</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
