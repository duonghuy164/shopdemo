<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' =>'required|min:2|unique:products,name_product',
            'price'=>'numeric',

        ];
    }
    public function messages(){
        return[
            'required' =>':attribute not null',
            'unique'=>':attribute not dupplicate',
             'min'=>':attribute min 2 character',
             'numeric'=>':attribute is numeric',


        ];
    }
    public function attributes(){
        return[
            'name' =>'name product',
            'price'=>'price of product',


        ];
    }
}
