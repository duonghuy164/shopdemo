<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    //
    protected $table = 'colors';
    protected $fillable =['name_color'];
    public function color(){
        return $this->hasMany('App\Models\Product','color_id','id');
    }
}
