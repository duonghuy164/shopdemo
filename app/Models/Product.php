<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'products';
    protected $fillable=['name_product','color_id','brand_id','price'];
    public function Brand(){
        return $this->belongsTo('App\Models\Brand');
    }
    public function Color(){
        return $this->belongsTo('App\Models\Color');
    }
}
