<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    //
    protected $table ='brands';
    protected $fillable=['brand_name','address',];
    public function product(){
        // lay ra cac san pham cung thuong hieu
        return $this->hasMany('App\Models\Product','brand_id','id');
    }
}
